import React, { useContext, useEffect, useState } from "react";
import { BooksContext } from "./BooksContext.js";
import { UserContext } from "./UserContext.js";
import { ReservationList } from "./ReservationList.js";
import PropTypes from 'prop-types';

/**
 * Component to use as part of Book Component
 * Get params from Book:
 * @param {object} book 
 * @param {Array.<Object>} reservations
 * @param {Array.<Object>} borrows
 * @returns React Components: ReservationBadge, ReservationButtons, ReservationList
 */
export function ReservationView({ book, reservations, borrows }) {
    const user = useContext(UserContext);
    const bookContext = useContext(BooksContext);
    const borrowed = borrows.length !== 0;
    const reserved = reservations.length !== 0;
    const [returnCode, setReturnCode] = useState(null);

    /**
     * Effect to hide book return code when view or user changes
     */
    useEffect(() => {
        setReturnCode(null);
    }, [bookContext.activeBook, user])

    /**
     * Conditions to show book status as span element
     * Book status can be: borrowed, reserved or available
     */
    function ReservationBadge() {
        if (borrowed && reserved) {
            const borrowEnd = borrows[0].end;
            const lastReserve = reservations[reservations.length - 1].end;
            return (
                <>
                    <span className="badge bg-danger me-1">Borrowed until: {new Date(borrowEnd).toLocaleString()}</span>
                    <span className="badge bg-warning text-dark">Reserved until: {new Date(lastReserve).toLocaleString()}</span>
                    {returnCode !== null ? <div className="alert alert-success fw-bold mt-2">Return code: {returnCode}</div> : null}
                </>
            )
        }

        if (borrowed) {
            const borrowEnd = borrows[0].end;
            return (
                <>
                    <span className="badge bg-danger">Borrowed until: {new Date(borrowEnd).toLocaleString()}</span>
                    {returnCode !== null ? <div className="alert alert-success fw-bold mt-2">Return code: {returnCode}</div> : null}
                </>
            )
        }

        if (reserved) {
            const lastReserve = reservations[reservations.length - 1].end
            return <span className="badge bg-warning text-dark">Reserved until: {new Date(lastReserve).toLocaleString()}</span>
        }

        return <span className="badge bg-success">Available</span>
    }

    /**
     * Conditions for borrow or reserve button that is shown to user,
     * also shows book return code for user, when book is borrowed
     */
    function ReservationButtons() {
        const borrowedByUser = borrows.find(borrow => borrow.user === user.name);
        const reservedByUser = reservations.find(reservation => reservation.user === user.name);

        if (borrowed) {
            if (reservedByUser || borrowedByUser) return null;
            return <button className="btn btn-primary mt-2" onClick={() => bookContext.addReservation(book.id)}>Reserve</button>
        }

        if (reserved) {
            /**
             * Gets book return code
             */
            const useReservationOnClick = async () => {
                const returnCode = await bookContext.useReservation(reservations[0].id)
                setReturnCode(returnCode);
            }

            const firstReservation = reservations[0].user;

            if (firstReservation === user.name) {
                return <button className="btn btn-primary mt-2" onClick={useReservationOnClick}>Borrow</button>
            }

            if (reservedByUser) return null;

            return <button className="btn btn-primary mt-2" onClick={() => bookContext.addReservation(book.id)}>Reserve</button>
        }

        /**
         * Gets book return code
         */
        const borrowBookOnClick = async () => {
            const returnCode = await bookContext.borrowBook(book.id)
            setReturnCode(returnCode);
        }

        return <button className="btn btn-primary mt-2" onClick={borrowBookOnClick}>Borrow</button>
    }

    return (
        <>
            <div><ReservationBadge /></div>
            <div><ReservationButtons /></div>
            <div><ReservationList borrows={borrows} reservations={reservations} borrowed={borrowed} reserved={reserved} /></div>
        </>
    );
}

/**
 * ReservationView PropTypes declaration
 */
ReservationView.propTypes = {
    book: PropTypes.object.isRequired,
    reservations: PropTypes.array.isRequired,
    borrows: PropTypes.array.isRequired
}