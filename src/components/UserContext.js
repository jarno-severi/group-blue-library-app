import React, { useState } from "react";
import PropTypes from 'prop-types';

export const USERROLE = "user";
export const ADMINROLE = "admin";

const USER = {
  name: "user",
  role: USERROLE
}

const USER2 = {
  name: "user2",
  role: USERROLE
}

const ADMIN = {
  name: "admin",
  role: ADMINROLE
}

export const UserContext = React.createContext(undefined);

/**
 * Provides state of user and buttons to change current user
 */
export const UserProvider = ({ children }) => {
  const [user, setUser] = useState(USER);

  return (
    <>
      <div className="button-group ms-5 mt-3">
        <input type="button" className="btn btn-sm btn-outline-secondary me-2" onClick={() => setUser(USER)} value="User" />
        <input type="button" className="btn btn-sm btn-outline-secondary me-2" onClick={() => setUser(USER2)} value="User 2" />
        <input type="button" className="btn btn-sm btn-outline-secondary" onClick={() => setUser(ADMIN)} value="Admin" />
      </div>
      <UserContext.Provider value={user}>
        {children}
      </UserContext.Provider>
    </>
  );
}

/**
 * UserProvider PropTypes declaration
 */
 UserProvider.propTypes = {
  children: PropTypes.element.isRequired
}