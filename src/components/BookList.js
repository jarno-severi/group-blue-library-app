import { useContext } from "react";
import { BooksContext } from "./BooksContext.js";
import { UserContext, ADMINROLE } from "./UserContext.js";
import PropTypes from 'prop-types';

/**
 * Show list of books and if those books are available, reserved or borrowed
 * @param {function} showAddBookView
 * @param {function} showBookView
 * @param {function} showEditView
 * @returns html elements
 */
export const BookList = ({ showAddBookView, showBookView, showEditView }) => {
    const context = useContext(BooksContext);
    const userContext = useContext(UserContext);

    /**
     * Checks if book is reserved or borrowed and return notification if it is
     * @param {Array.<Object>} reservations of the current book
     * @param {Array.<Object>} borrows of the current book
     * @returns html element ot book current state
     */
    const ReservationStatus = ({ reservations, borrows }) => {
        const reservationsAmount = reservations.length
        const borrowsAmount = borrows.length

        return (
            <>
                {(reservationsAmount === 0 && borrowsAmount === 0) ? <div className="alert alert-success">Available</div> : ""}
                {(reservationsAmount > 0 && borrowsAmount === 0) ? <div className="alert alert-warning">Reserved</div> : ""}
                {(borrowsAmount > 0) ? <div className="alert alert-danger">Borrowed</div> : ""}
            </>)
    }

    return (
        <div className="row">
            <div className={userContext.role === ADMINROLE ? "card col-xxl-4" : "d-none"}>
                <div className="card-body d-flex align-items-center justify-content-center flex-column">
                    <h1 className="card-title text-muted display-1">+</h1>
                    <h5 className="card-subtitle mb-2 text-muted">New book</h5>
                    <div className="card-footer">
                        <button onClick={showAddBookView} className="align-self-end btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
            {context.bookList.map(book => {
                return (
                    <div key={book.isbn} className="card col-xxl-4">
                        <div className="card-body">
                            <img src={`https://picsum.photos/seed/${book.title}/150/220`} className="img-thumbnail rounded mx-auto d-block mb-1" alt="Book" />
                            <h4 className="card-title">{book.title}</h4>
                            <h5 className="card-subtitle mb-2 text-muted">{book.author}</h5>
                            <ReservationStatus reservations={book.reservations} borrows={book.borrows} />
                            <div className="card-footer">
                                <button onClick={() => showBookView(book.id)} className="align-self-end btn btn-primary">View</button>
                                {userContext.role === ADMINROLE ? <button onClick={() => showEditView(book.id)} className="ms-2 align-self-end btn btn-primary">Edit</button> : null}
                            </div>
                        </div>
                    </div>
                )
            }
            )}
        </div>
    )
}

 BookList.propTypes = {
    showAddBookView: PropTypes.func.isRequired,
    showBookView: PropTypes.func.isRequired,
    showEditView: PropTypes.func.isRequired,
}