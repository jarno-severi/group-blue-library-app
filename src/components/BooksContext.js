import React, {useState, useEffect, useContext } from "react";
import PropTypes from 'prop-types';
import { UserContext } from "./UserContext.js";

export const BooksContext = React.createContext(undefined);
const BASEURL = "http://localhost:3004";

/**
 * Provides state of book list and active book.
 * Also functions to modify the state and database
 */
export const BooksProvider = ({ children }) => {
    const [bookList, setBookList] = useState([]);
    const [activeBook, setActiveBook] = useState(null);
    const userContext = useContext(UserContext);

    // Filter reservations and borrows that are not active anymore
    const filterBookReservationsAndBorrows = (book) => {
        const now = new Date();

        const reservations = []; 
        for(const reservation of book.reservations){
            if (reservation.used === null) {
                const end = new Date(reservation.end);
                if (end > now){
                    reservations.push(reservation);
                }
            }
        }

        const borrows = [];
        for(const borrow of book.borrows){
            if (borrow.returned === null) {
                borrows.push(borrow);
            }
        }

        book.reservations = reservations;
        book.borrows = borrows;
        return book;
    }

    /**
     * Get all books from the server
     */
    const getBooks = async () => {
        try {
            const response = await  fetch(`${BASEURL}/books/?_embed=reservations&_embed=borrows`);
            if (response.status !== 200) return;
            
            const books = await response.json();
            // Process book data
            for (let i = 0; i < books.length; i++) {
                books[i] = filterBookReservationsAndBorrows(books[i]);
            };

            // Add to local state
            setBookList(books);
        } catch(error) {
            console.log(error);
            Error("Failed to fetch books");
        }
    }
  
    /**
     * Get book by id
     * @param {id} book id to fetch 
     */
    const getBook = async (id) => {
        try {
            const response = await fetch(`${BASEURL}/books/${id}?_embed=reservations&_embed=borrows`);
            if (response.status !== 200) return;
            
            let book = await response.json();
            book = filterBookReservationsAndBorrows(book);
            setActiveBook(book);
        } catch(error) {
            console.log(error);
            Error("Failed to fetch book");
        }
    }

    /**
     * Add book and update state
     * @param {book} book to add 
     */
    const addBook = async (book) => {
        try {
            const response = await fetch(`${BASEURL}/books/`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(book)
            });
            if (response.status !== 201) return;
            
            const newBook = await response.json();
            newBook.reservations = [];
            newBook.borrows = [];
            
            // Add book to list
            setBookList([...bookList, newBook]);
        } catch(error) {
            console.log(error);
            Error("Failed to add book");
        }
    }
  
    /**
     * Update book and update state
     * @param {book} book to update 
     */
    const updateBook = async (book) => {
        try {
            const response = await fetch(`${BASEURL}/books/${book.id}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(book)
            });

            if (response.status !== 200) return;

            // Get updated book and reservations/borrows
            const bookResponse = await fetch(`${BASEURL}/books/${book.id}?_embed=reservations&_embed=borrows`);

            if (bookResponse.status !== 200) return;
            
            let updatedBook = await bookResponse.json();
            updatedBook = filterBookReservationsAndBorrows(updatedBook);
            
            // Replace updated book on the list and update state
            const books = bookList.map(book => book.id === updatedBook.id ? updatedBook : book);
            setActiveBook(updatedBook);
            setBookList(books);
        } catch(error) {
            console.log(error);
            Error("Failed to update book");
        }
    }
  
    /**
     * Delete book and update state
     * @param {id} book id to delete
     */
     const removeBook = async (id) => {
        try {
            const response = await fetch(`${BASEURL}/books/${id}`, {
                method: "DELETE"
            });

            if (response.status !== 200) return;

            // Remove book from state
            const books = bookList.filter(book => book.id !== id);
            setBookList(books);

            // Reset active book
            setActiveBook(null);
        } catch(error) {
            console.log(error);
            Error("Failed to remove book");
        }
    }
  
    /**
     * Add reservation to book
     * @param {book} book to reserve 
     */
    const addReservation = async (bookId) => {
        const book = bookList.find(book => book.id === bookId);
        let nextAvailableTime = new Date();

        // If borrowed, reservation starts when borrow ends
        if (book.borrows.length !== 0){
            nextAvailableTime = new Date(book.borrows[0].end);
        }

        // If there is also reservations, next reservation end is start for new
        if (book.reservations.length !== 0) {
            const lastReservation = book.reservations.reduce((reservation,previous) => reservation);
            nextAvailableTime = new Date(lastReservation.end);
        }

        const reservationEnd = new Date(nextAvailableTime.getTime() + (3*60*60*1000)) // +3h
        const reservation = {
            "user": userContext.name,
            "bookId": bookId,
            "start": nextAvailableTime.toISOString(),
            "end": reservationEnd.toISOString(),
            "used": null
        }

        try {
            const response = await fetch(`${BASEURL}/reservations/`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(reservation)
            });

            if (response.status !== 201) return;

            const newReservation = await response.json();

            // Add to book reservations
            const updatedBook = bookList.find(book => book.id === bookId);
            if (updatedBook === null) return;
            updatedBook.reservations.push(newReservation);

            // Trigger book update
            const books = bookList.map(book => book.id === bookId ? updatedBook : book);
            setBookList(books);
            setActiveBook(updatedBook);
        } catch(error) {
            console.log(error);
            Error("Failed to reserve book");
        };
    }

    /**
     * User reservation to borrow book
     * @param {reservationId} id which to use to borrow book
     */
    const useReservation = async (reservationId) => {
        // Mark reservation used
        const reservation = activeBook.reservations.find(reservation => reservation.id === reservationId);
        reservation.used = new Date().toISOString();

        try {
            const response = await fetch(`${BASEURL}/reservations/${reservationId}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(reservation)
            });

            if (response.status !== 200) return;

            // Create new borrow
            const now = new Date();
            const borrowEnd = new Date(now.getTime() + (3*60*60*1000)) // +3h
            const borrow = {
                "user": userContext.name,
                "bookId": reservation.bookId,
                "code": Math.floor(1000 + Math.random() * 9000),
                "start": now.toISOString(),
                "end": borrowEnd.toISOString(),
                "returned": null
            }
            
            const borrowResponse = await fetch(`${BASEURL}/borrows/`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(borrow)
            });

            if (borrowResponse.status !== 201) return;

            const newBorrow = await borrowResponse.json();

            const updatedBook = bookList.find(book => book.id === reservation.bookId);

            // Add borrow to book borrows
            updatedBook.borrows.push(newBorrow);

            // Remove reservation form book
            const reservations = updatedBook.reservations.filter(reservation => reservation.id !== reservationId);
            updatedBook.reservations = reservations;

            // Trigger book update
            const books = bookList.map(book => book.id === updatedBook.id ? updatedBook : book);
            setBookList(books);
            setActiveBook(updatedBook);
            return newBorrow.code;
        } catch(error) {
            console.log(error);
            Error("Failed to return book");
        }
    }

    /**
     * Remove reservation from book
     * @param {reservationId} borrow id to remove
     */
     const removeBookReservation = async (reservationId) => {
        try {
            const response = await fetch(`${BASEURL}/reservations/${reservationId}`, {
                method: "DELETE",
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (response.status !== 200) return;

            // Remove reservation from book
            const updatedBook = activeBook;
            const reservations = updatedBook.reservations.filter((reservation) => reservation.id !== reservationId);
            updatedBook.reservations = reservations;

            // Trigger book update
            const books = bookList.map(book => book.id === updatedBook.id ? updatedBook : book);
            setBookList(books);
            setActiveBook(updatedBook);
        } catch(error) {
            console.log(error);
            Error("Failed to remove borrow from book");
        }
    }

    /**
     * Add borrow to book
     * @param {book} book to borrow 
     */
     const borrowBook = async (bookId) => {
        const now = new Date();
        const borrowEnd = new Date(now.getTime() + (3*60*60*1000)) // +3h
        const borrow = {
            "user": userContext.name,
            "bookId": bookId,
            "code": Math.floor(1000 + Math.random() * 9000),
            "start": now.toISOString(),
            "end": borrowEnd.toISOString(),
            "returned": null
        }

        try {
            const response = await fetch(`${BASEURL}/borrows/`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(borrow)
            });

            if (response.status !== 201) return;

            const newBorrow = await response.json();

            // Add to book borrows
            const updatedBook = bookList.find(book => book.id === bookId);
            if (updatedBook === null) return;
            updatedBook.borrows.push(newBorrow);

            // Trigger book update
            const books = bookList.map(book => book.id === bookId ? updatedBook : book);
            setBookList(books);
            setActiveBook(updatedBook);
            return newBorrow.code;
        } catch(error) {
            console.log(error);
            Error("Failed to borrow book");
        }
    }

    /**
     * Remove borrow from book
     * @param {borrowId} borrow id to remove
     */
     const removeBookBorrow = async (borrowId) => {
        try {
            const response = await fetch(`${BASEURL}/borrows/${borrowId}`, {
                method: "DELETE",
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (response.status !== 200) return;

            // Remove borrow from book
            const updatedBook = activeBook;
            const borrows = updatedBook.borrows.filter(borrow => borrow.id !== borrowId);
            updatedBook.borrows = borrows;

            // Trigger book update
            const books = bookList.map(book => book.id === updatedBook.id ? updatedBook : book);
            setBookList(books);
            setActiveBook(updatedBook);
        } catch(error) {
            console.log(error);
            Error("Failed to remove borrow from book");
        }
    }

    /**
     * Return book
     * @param {borrowId} borrow id to return
     */
     const returnBook = async (borrowId) => {
        const borrow = activeBook.borrows.find((borrow => borrow.id === borrowId));
        borrow.returned = new Date().toISOString();

        try {
            const response = await fetch(`${BASEURL}/borrows/${borrowId}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(borrow)
            });

            if (response.status !== 200) return;

            // Remove borrow from book
            const updatedBook = activeBook;
            const borrows = updatedBook.borrows.filter(borrow => borrow.id !== borrowId);
            updatedBook.borrows = borrows;
            
            // Trigger book update
            const books = bookList.map(book => book.id === updatedBook.id ? updatedBook : book);
            setBookList(books);
            setActiveBook(updatedBook);
        }
        catch(error) {
            console.log(error);
            Error("Failed to return book");
        }
    }

    // Run once at start to get books
    useEffect(() => {
      getBooks();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
  
    /**
     * Set active book to state
     * @param {id} book id 
     */
    const selectBook = (id) => {
      const book = bookList.find(book => book.id === id);
      if (book !== null) setActiveBook(book);
    }

    /**
     * Set active book to null
     */
    const unselectBook = () => {
        setActiveBook(null);
    }
    
    // Context properties 
    const booksContext = {
        bookList: bookList,
        activeBook: activeBook,
        selectBook: selectBook,
        unselectBook: unselectBook,
        getBooks: getBooks,
        getBook: getBook,
        addBook: addBook,
        updateBook: updateBook,
        removeBook: removeBook,
        addReservation: addReservation,
        removeBookReservation: removeBookReservation,
        useReservation: useReservation,
        borrowBook: borrowBook,
        removeBookBorrow: removeBookBorrow,
        returnBook: returnBook
    }

    return (
      <>
        <BooksContext.Provider value={booksContext}>
            {children}
        </BooksContext.Provider>
      </>
    );
}

/**
 * UserProvider PropTypes declaration
 */
BooksProvider.propTypes = {
    children: PropTypes.element.isRequired
}