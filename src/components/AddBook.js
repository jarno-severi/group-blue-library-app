import React, { useEffect, useState, useContext } from 'react'
import { UserContext, ADMINROLE } from "./UserContext.js";
import { BooksContext } from "./BooksContext.js";
import PropTypes from 'prop-types';

/**
 * Initial state for the form used in AddBook-component
 */
const formInitialState = {
    title: "",
    isbn: "",
    subtitle: "",
    author: "",
    published: "",
    publisher: "",
    pages: "",
    description: "",
    website: ""
}

/**
 * Provides functions to add, modify and delete books
 */
export function AddBook ( { addBookViewVisible, hideAddBookView } ) {
    // Contexts for user and books
    const userContext = useContext(UserContext);
    const bookContext = useContext(BooksContext);

    /**
     * editView: state for the mode of the component
     * false - Mode for adding a book
     * true - Mode for modifying / deleting a book
     */
    const [editView, setEditView] = useState(false);
    
    // States for form fields and errors from their validation
    const [formErrors, setFormErrors] = useState(formInitialState);
    const [formState, setFormState] = useState(formInitialState);

    // If there is an activeBook set, this component will function in modifying / deleting mode (for that activeBook)
    // If there is no activeBook set, this component will function in add new book -mode
   useEffect(() => { 
        if (bookContext.activeBook && bookContext.activeBook !== null) {
            setFormState({
                title: bookContext.activeBook.title,
                isbn: bookContext.activeBook.isbn,
                subtitle: bookContext.activeBook.subtitle,
                author: bookContext.activeBook.author,
                published: bookContext.activeBook.published,
                publisher: bookContext.activeBook.publisher,
                pages: bookContext.activeBook.pages,
                description: bookContext.activeBook.description,
                website: bookContext.activeBook.website
            });
            // Set view for editing a book
            setEditView(true);
            setFormErrors(formInitialState);
        } else {
            setEditView(false);
            setFormErrors(formInitialState);
            setFormState(formInitialState);
            
        }
    }, [bookContext.activeBook]);

    /**
     * Validates form inputs for erronous values
     * Returns false if errors are found, true if not
     * Updates formErrors-state accordingly
     * @returns boolean
     */
    const validateFormInputs = () => {
        const validationErrors = {};
        
        if (!formState.title || /^\s*$/.test(formState.title)) {
            validationErrors.title = "Title is required";
        }

        // ISBN can only be set while adding a new book
        if (editView === false) {
            if (!formState.isbn || /^\s*$/.test(formState.isbn)) {
                validationErrors.isbn = "Isbn is required";
            } else if (isNaN(formState.isbn)) {
                validationErrors.isbn = "Isbn must be numeric";
            } else if (formState.isbn <= 0) {
                validationErrors.isbn = "Isbn must greater than 0";
            } else if (!(formState.isbn.length === 10 || formState.isbn.length === 13)) {
                validationErrors.isbn = "Isbn length must be 10 or 13";
            }
        }

        if (!formState.author || /^\s*$/.test(formState.author)) {
            validationErrors.author = "Author is required";
        }

        if (!formState.published || /^\s*$/.test(formState.published)) {
            validationErrors.published = "Publish date is required";
        }

        if (!formState.publisher || /^\s*$/.test(formState.publisher)) {
            validationErrors.publisher = "Publisher is required";
        }

        if (!formState.pages || /^\s*$/.test(formState.pages)) {
            validationErrors.pages = "Pages is required";
        } else if (isNaN(formState.pages)) {
            validationErrors.pages = "Pages must be numeric";
        } else if (formState.pages <= 0) {
            validationErrors.pages = "Pages must greater than 0";
        }

        if (!formState.description || /^\s*$/.test(formState.description)) {
            validationErrors.description = "Description is required";
        }

        // If there are errors, update formErrors-state
        if (Object.keys(validationErrors).length === 0) { 
            return true;
        } else {
            setFormErrors(validationErrors);
            return false;
        }
    }

    /**
     * Handles changes on all fields of the form
     * Updates formState accordingly
     * @param {event} - html event
     */
    const handleInputChange = (event) => {
        // Name of any field is same as id of that field
        const inputName = event.target.id;
        const inputValue = event.target.value;
        setFormState({...formState, [inputName]: inputValue});
    }

    /**
     * Handles saving changes after modifying books information
     * - Inputs must pass validation
     */
    const handleEditBookSaveClick = () => {
        if (!validateFormInputs()) return;

        const editedBook = {
            id: bookContext.activeBook.id,
            title: formState.title,
            isbn: formState.isbn,
            subtitle: formState.subtitle,
            author: formState.author,
            published: formState.published,
            publisher: formState.publisher,
            pages: formState.pages,
            description: formState.description,
            website: formState.website
        }
        bookContext.updateBook(editedBook);

        // Reset form errors
        setFormErrors(formInitialState);
    }

    /**
     * Handles saving a new book
     * - Inputs must pass validation
     */
    const handleNewBookSaveClick = () => {
        if (!validateFormInputs()) return;
        
        const newBook = {
            title: formState.title,
            isbn: formState.isbn,
            subtitle: formState.subtitle,
            author: formState.author,
            published: formState.published,
            publisher: formState.publisher,
            pages: formState.pages,
            description: formState.description,
            website: formState.website
        }
        bookContext.addBook(newBook);

        // Reset form and errors
        setFormErrors(formInitialState);
        setFormState(formInitialState);
    }

    /**
     * Handles deleting a book
     */
    const handleDeleteBookClick = () => {
        const confirmed = window.confirm("Are you sure?");
        if (!confirmed) return;

        bookContext.removeBook(bookContext.activeBook.id);
        
        // Reset form and errors
        setFormErrors(formInitialState);
        setFormState(formInitialState);
        
        // Return component to "add book" -mode
        closeView();
    }

    /**
     * Closes the view of AddBook component
     * - Closes both add book -view and edit/delete book -view
     */
    const closeView = () => {
        setEditView(false);
        hideAddBookView();
    }
    
    return (
        <div className={userContext.role !== ADMINROLE || (!addBookViewVisible && !editView) ? "d-none" : "card p-3"}>
            <h2 className="display-6">{editView ? "Edit book" : "Add new book"}</h2>
            <form>
                <div className="form-group">
                    <label htmlFor="title">Title</label>
                    <div className="text-danger">{formErrors.title}</div>
                    <input 
                        id="title"
                        value={formState.title}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3" 
                        placeholder="Title" />
                </div>
                <div className="form-group">
                    <label htmlFor="isbn">ISBN</label>
                    <div className="text-danger">{formErrors.isbn}</div>
                    <input 
                        disabled={editView}
                        id="isbn" 
                        value={formState.isbn}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3" 
                        placeholder="ISBN" />
                </div>
                <div className="form-group">
                    <label htmlFor="subtitle">Subtitle</label>
                    <div className="text-danger">{formErrors.subtitle}</div>
                    <input 
                        id="subtitle" 
                        value={formState.subtitle}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3"
                        placeholder="Subtitle" />
                </div>
                <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <div className="text-danger">{formErrors.description}</div>
                    <textarea 
                        id="description" 
                        value={formState.description}
                        onChange={handleInputChange}
                        type="text"
                        rows="5"  
                        className="form-control mb-3" 
                        placeholder="Description" />
                </div>
                <div className="form-group">
                    <label htmlFor="author">Author</label>
                    <div className="text-danger">{formErrors.author}</div>
                    <input 
                        id="author" 
                        value={formState.author}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3"
                        placeholder="Author" />
                </div>
                <div className="form-group">
                    <label htmlFor="pages">Pages</label>
                    <div className="text-danger">{formErrors.pages}</div>
                    <input 
                        id="pages" 
                        value={formState.pages}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3" 
                        placeholder="Pages" />
                </div>
                <div className="form-group">
                    <label htmlFor="published">Publish date</label>
                    <div className="text-danger">{formErrors.published}</div>
                    <input 
                        id="published"
                        value={formState.published}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3" 
                        placeholder="Publish date" />
                </div>
                <div className="form-group">
                    <label htmlFor="publisher">Publisher</label>
                    <div className="text-danger">{formErrors.publisher}</div>
                    <input 
                        id="publisher"
                        value={formState.publisher}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3"
                        placeholder="Publisher" />
                </div>
                <div className="form-group">
                    <label htmlFor="website">Website</label>
                    <div className="text-danger">{formErrors.website}</div>
                    <input 
                        id="website"
                        value={formState.website}
                        onChange={handleInputChange}
                        type="text" 
                        className="form-control mb-3"
                        placeholder="Website" />
                </div>
                
                {editView 
                    ? <button type="button" className="btn btn-primary m-0 me-2" onClick={handleEditBookSaveClick}>Save changes</button>
                    : <button type="button" className="btn btn-primary m-0 me-2" onClick={handleNewBookSaveClick}>Save book</button>
                }
                {editView 
                    ? <button type="button" className="btn btn-danger me-2" onClick={handleDeleteBookClick}>Delete book</button>
                    : null
                }
                <button type="button" className="btn btn-outline-secondary me-2" onClick={closeView}>Close</button>
            </form>
        </div>
    )
}

/**
 * Proptypes for AddBook
 */
AddBook.propTypes = {
    hideAddBookView: PropTypes.func.isRequired,
    addBookViewVisible: PropTypes.bool.isRequired
}