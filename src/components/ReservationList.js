import React, { useContext } from "react";
import { BooksContext } from "./BooksContext.js";
import { UserContext, ADMINROLE } from "./UserContext.js";
import PropTypes from 'prop-types';

/**
 * Component to use as part of ReservationView Component
 * Gets params from ReservationView:
 * @param {Array.<Object>} reservations
 * @param {Array.<Object>} borrows
 * @param {boolean} borrowed
 * @param {boolean} reserved
 * @returns React Components: BorrowsList and/or ReservationList
 */
export function ReservationList({ reservations, borrows, borrowed, reserved }) {
    const user = useContext(UserContext);
    const bookContext = useContext(BooksContext);

    /**
     * Remove borrow after confirmation
     * @param {string} item -Borrow id
     */
    function handleRemoveBorrow(item) {
        const confirmed = window.confirm("Are you sure?");
        if (confirmed) bookContext.removeBookBorrow(item);
    }

    /** 
     * Borrow list for admin user with delete possibility
     */
    function BorrowsListDOM() {
        return (
            <ol className="list-group list-group-numbered mt-2">
                <li className="list-group-item d-flex justify-content-between align-items-start">
                    <div className="ms-2 me-auto">
                        <div className="fw-bold">Borrow #{borrows[0].user}</div>
                        Start: {new Date(borrows[0].start).toLocaleString()} <br />
                        End: {new Date(borrows[0].end).toLocaleString()} <br />
                        Return code: {borrows[0].code} 
                    </div>
                    <button
                        className="btn btn-danger badge bg-danger rounded-pill"
                        onClick={() => handleRemoveBorrow(borrows[0].id)}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash3" viewBox="0 0 16 16">
                            <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z" />
                        </svg>
                    </button>
                </li>
            </ol>
        );
    }

    /**
     * Remove reservation after confirmation
     * @param {string} item -Reservation id
     */
    function handleRemoveReservation(item) {
        const confirmed = window.confirm("Are you sure?");
        if (confirmed) bookContext.removeBookReservation(item)
    }

    /** 
     * Seperate reservations list for admin role and user role 
     * Admin role can remove reservation from the list item
     */
    function ReservationListDOM() {
        return (
            <ol className="list-group list-group-numbered mt-2">
                {reservations.map(reservation => {
                    return (
                        <li key={"reservation" + reservation.id} className="list-group-item d-flex justify-content-between align-items-start">
                            <div className="ms-2 me-auto">
                                <div className="fw-bold">Reservation #{reservation.user}</div>
                                Start: {new Date(reservation.start).toLocaleString()} <br />
                                End: {new Date(reservation.end).toLocaleString()}
                            </div>
                            {user.role === ADMINROLE ? <button
                                className="btn btn-danger badge bg-danger rounded-pill"
                                onClick={() => handleRemoveReservation(reservation.id)}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash3" viewBox="0 0 16 16">
                                    <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z" />
                                </svg>
                            </button> : null}
                        </li>
                    );
                })}
            </ol>
        )
    }

    /** 
     * DOM Render logic 
     */
    if (borrowed && reserved) return (
        <>
            {user.role === ADMINROLE ? <BorrowsListDOM /> : null}
            <ReservationListDOM />
        </>
    );
    
    if (borrowed && user.role === ADMINROLE) return <BorrowsListDOM />
    
    if (reserved) return <ReservationListDOM />
    
    return null;
}

/**
 * ReservationList PropTypes declaration
 */
ReservationList.propTypes = {
    reservations: PropTypes.array.isRequired,
    borrows: PropTypes.array.isRequired,
    borrowed: PropTypes.bool.isRequired,
    reserved: PropTypes.bool.isRequired
}