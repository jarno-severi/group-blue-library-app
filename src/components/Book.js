import React, { useContext } from "react";
import { BooksContext } from "./BooksContext.js";
import { ReturnBook } from "./ReturnBook.js"
import { ReservationView } from "./ReservationView.js";
import { UserContext, ADMINROLE } from "./UserContext.js";
import PropTypes from 'prop-types';

/**
 * Renders information and components for selected book (activeBook)
 * Gets param from Library:
 * @param {function} showEditView 
 * @returns html element, ReservationView and ReturnBook components
 */
export const Book = ({showEditView}) => {
    const bookContext = useContext(BooksContext);
    const book = bookContext.activeBook;
    const user = useContext(UserContext);

    /**
     * In case of no book is selected, React needs something to be returned
     */
     if (book === null) return null;

    /**
     * Removes book after confimation
     * @param {string} id -Book id
     */
    function handleRemoveBook(id) {
        const confirmed = window.confirm("Are you sure?");
        if (confirmed) bookContext.removeBook(id);
    }

    return (
        <div className="card">
            <div className="card-body">
                <div className="float-end align-middle mb-2">
                    {user.role === ADMINROLE ?
                        <>
                            <button type="button" className="btn btn-primary btn-sm me-2" aria-label="Edit" onClick={() => showEditView(book.id)}>
                                Edit
                            </button>
                            <button type="button" className="btn btn-danger btn-sm me-2" aria-label="Delete" onClick={() => handleRemoveBook(book.id)}>
                                Delete
                            </button>
                        </> : null}
                    <button type="button" className="btn btn-outline-secondary btn-sm" aria-label="Close" onClick={() => bookContext.unselectBook()}>
                        Close
                    </button>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"title" + book.id}>Title</span>
                    <span className="form-control">{book.title}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"isbn" + book.id}>ISBN</span>
                    <span className="form-control">{book.isbn}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"subtitle" + book.id}>Subtitle</span>
                    <span className="form-control">{book.subtitle}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"description" + book.id}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-info-lg" viewBox="0 0 16 16">
                            <path d="m9.708 6.075-3.024.379-.108.502.595.108c.387.093.464.232.38.619l-.975 4.577c-.255 1.183.14 1.74 1.067 1.74.72 0 1.554-.332 1.933-.789l.116-.549c-.263.232-.65.325-.905.325-.363 0-.494-.255-.402-.704l1.323-6.208Zm.091-2.755a1.32 1.32 0 1 1-2.64 0 1.32 1.32 0 0 1 2.64 0Z" />
                        </svg>
                    </span>
                    <span className="form-control">{book.description}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"author" + book.id}>Author</span>
                    <span className="form-control">{book.author}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"pages" + book.id}>Pages</span>
                    <span className="form-control">{book.pages}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"publisher" + book.id}>Publisher</span>
                    <span className="form-control">{book.publisher}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"published" + book.id}>Published</span>
                    <span className="form-control">{new Date(book.published).toLocaleString()}</span>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id={"website" + book.id}>Website</span>
                    <span className="form-control">{book.website}</span>
                </div>
                <ReservationView book={book} reservations={book.reservations} borrows={book.borrows} />
                {(book.borrows.length !== 0 && book.borrows[0].user === user.name) ? <ReturnBook /> : null}
            </div>
        </div>
    )
}

/**
 * Book PropTypes declaration
 */
Book.propTypes = {
    showEditView: PropTypes.func.isRequired,
}