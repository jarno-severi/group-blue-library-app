import { AddBook } from "./AddBook.js";
import { Book } from "./Book.js";
import { BookList } from "./BookList.js";
import React, { useState, useContext, useEffect } from 'react'
import { BooksContext } from "./BooksContext.js";
import { UserContext } from "./UserContext.js";

/**
 * View for the library and its subcomponents
 * - Provides states for visibility of Book and AddBook components
 */
export function Library() {
    // AddBook-component is for both adding a book and modifying / deleting books
    const [addBookViewVisible, setAddBookViewVisible] = useState(false);
    // bookView is view of the book, without editing capabilities
    const [bookViewVisible, setBookViewVisible] = useState(false);

    const bookContext = useContext(BooksContext);
    const user = useContext(UserContext);
    
    // When user changes, library view must be reseted
    useEffect(() => {
        bookContext.unselectBook();
        setAddBookViewVisible(false);
        setBookViewVisible(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user]);

    /**
     * Sets AddBook component visible (for adding a new book)
     * - Unselects current activeBook, which sets AddBook component to mode for adding a new book
     */
    const showAddBookView = () => {
        setAddBookViewVisible(true);
        bookContext.unselectBook();
    }

     /**
     * Sets AddBook component visible (for modifying or deleting a book)
     * - Sets current activeBook, which sets AddBook component to mode for modifying and deleting books
     * - Hides bookView
     * @param {bookId} - Id of the current activeBook 
     */
    const showEditView = (bookId) => {
        bookContext.selectBook(bookId);
        setAddBookViewVisible(true);
        setBookViewVisible(false);
    }

    /**
     * Hides AddBook component
     */
    const hideAddBookView = () => {
        setAddBookViewVisible(false);
        bookContext.unselectBook();
    }

    /**
     * Set bookView visible and set activeBook for it
     * - Hides AddBook component
     * @param {bookId} - Id of the current activeBook 
     */
    const showBookView = (bookId) => {
        bookContext.selectBook(bookId);
        setBookViewVisible(true);
        setAddBookViewVisible(false);
    }

    return (
        <>
            <div className="px-5 pb-5">
                <h1 className="text-center text-primary mb-4">Group Blue Library</h1>
                <div className="row">
                    <div className="col-lg-6 order-lg-2">
                        {bookViewVisible && <Book showEditView={showEditView} />}
                        {addBookViewVisible && <AddBook addBookViewVisible={addBookViewVisible} hideAddBookView={hideAddBookView} />}
                    </div>
                    <div className={bookContext.activeBook === null && !addBookViewVisible ? "col-12" : "col-lg-6 order-lg-1"}>
                        <BookList showAddBookView={showAddBookView} showBookView={showBookView} showEditView={showEditView}/>
                    </div>
                </div>
                <div className="footer text-center text-muted mt-2">Copyright Group Blue</div>
            </div>
        </>
    )
}