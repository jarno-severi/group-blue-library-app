import { useState, useContext } from "react";
import { BooksContext } from "./BooksContext.js";

/** 
 * User can return book by given code.
 */
export const ReturnBook = () => {
    const [value, setValue] = useState("");
    const [alert, setAlert] = useState("");

    const context = useContext(BooksContext);
    const book = context.activeBook;
    const codes = book.borrows.map(borrow => borrow.code)

    /**
     * Checks if borrowed book is founded by digit-code from input value and returns it if founded
     * @param {e} html event
     * @returns 
     */
    const handleSubmit = e => {
        e.preventDefault();
        setAlert("");
        const borrow = book.borrows.filter(borrow => borrow.code === parseInt(value));
        const code = codes.filter(code => code === parseInt(value));

        if (code.length === 0) {
            return (
                setAlert(<div className="alert alert-danger">You have entered wrong code!</div>)
            );
        } else {
            context.returnBook(borrow[0].id);
            return (
                setAlert(<div className="alert alert-success">Book returned succesfully</div>)
            );
        }
    }

    return (
        <form onSubmit={handleSubmit} className="form row g-1" >
            <div className="col-6">
                <input type="text"
                    className="form-control mt-2 col-auto"
                    value={value}
                    onChange={e => setValue(e.target.value)}
                    placeholder="Write code E.g. '1234'"
                    aria-describedby="codeHelpBlock"
                />
            </div>
            <div className="col-6">
                <button type="submit" className="align-self-end btn btn-primary mt-2">Return book</button>
            </div>
            <div id="codeHelpBlock" className="form-text">
                Return borrowed book by 4-digit number code and press Return book.
            </div>
            <>{alert}</>
        </form>
    )
}