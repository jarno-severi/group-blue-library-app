import './App.css';
import React from "react";
import { BooksProvider } from "./components/BooksContext.js";
import { UserProvider } from "./components/UserContext.js";
import { Library } from './components/Library';

function App() {
  return (
    <>
      <UserProvider>
        <BooksProvider>
          <Library />
        </BooksProvider>
      </UserProvider>
    </>
  );
}

export default App;
